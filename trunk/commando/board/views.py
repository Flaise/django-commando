import random

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from board.models import Board, Piece, Color

def view_abandon(request, board_id): ####################################### TODO: don't abandon a game that isn't yours
    if not request.user.is_authenticated():
        # usually only if session expires
        messages.error(request, 'Please log in.')
        return redirect('board:index')
        
    if request.method != 'POST':
        return redirect('board:index')
        
    board = Board.objects.get(id=board_id)
    board.delete()
    
    messages.success(request, 'Game ended successfully.')
    return redirect('board:index')
    
def view_newgame(request):
    if not request.user.is_authenticated():
        # usually only if session expires
        messages.error(request, 'Please log in.')
        return redirect('board:index')
        
    if request.method != 'POST':
        return redirect('board:index')
        
    opponents = User.objects.filter(username=request.POST['opponent_username'])
    if len(opponents) == 0:
        messages.error(request, 'No such user.')
        return redirect('board:index')
    opponent = opponents[0]
        
    board = Board()
    selected_color = request.POST['my_color']
    if selected_color == 'orange':
        board.player_orange = request.user
        board.player_blue = opponent
    elif selected_color == 'blue':
        board.player_orange = opponent
        board.player_blue = request.user
    elif selected_color == 'random':
        if random.random() < .5:
            board.player_orange = request.user
            board.player_blue = opponent
        else:
            board.player_orange = opponent
            board.player_blue = request.user
    else:
        messages.error(request, 'Select a color to play as.')
        return redirect('board:index')
            
    board.populate_and_save()

    return redirect('board:board', board.id)
        
def view_logout(request):
    if request.method == 'POST': # Is this necessary? Might be possible to grief users by logging them out from off-site.
        logout(request)
    return redirect('board:index')
    
def view_login(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            if user.is_active:
                login(request, user)
                #messages.success(request, 'Logged in.') # already obvious from index page
            else:
                messages.error(request, 'That account is disabled.')
        else:
            messages.error(request, 'Invalid username/password combination.')
            
    return redirect('board:index')
    
def view_register(request):
    if request.method == 'POST':
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        
        if password1 != password2:
            messages.error(request, "The given passwords don't match.")
        else:
            user = User.objects.create_user(request.POST['username'], None, password1)
            login(request, user)
            messages.success(request, 'Account created successfully.')
    
    return redirect('board:index')
        
def view_index(request):
    username = None
    if request.user.is_authenticated():
        username = request.user.username

    return render(request, 'board/index.html', { 'username':username, 'board_list':Board.objects.all() })
        
def view_make_move(request, board_id):
    if request.method == 'POST':
        board = get_object_or_404(Board, pk=board_id)
        from_x = request.POST['from_x']
        from_y = request.POST['from_y']
        to_x = request.POST['to_x']
        to_y = request.POST['to_y']
        
        success, fail_reason = board.try_move_to(request.user, from_x, from_y, to_x, to_y)
        if not success:
            messages.error(request, fail_reason)
        return redirect('board:board', board_id)
    else:
        # sometimes happens when recovering from an error
        return redirect('board:board', board_id)
        
def view_board(request, board_id):
    board = get_object_or_404(Board, pk=board_id)
    
    rows = []
    for y in range(0, board.get_height()):
        rows.append([])
        for x in range(0, board.get_width()):
            pieces = board.get_pieces_at(x, y)
            if len(pieces) == 0:
                rows[y].append(None)
            else:
                piece = pieces[0]
                if piece.color == Color.ORANGE:
                    color = 'orange'
                elif piece.color == Color.BLUE:
                    color = 'blue'
                else:
                    raise Exception('Invalid piece owner')
                    
                #if board.color_to_move == Color.ORANGE:
                #    to_move = 'Orange to move.'
                #elif board.color_to_move == Color.BLUE:
                #    to_move = 'Blue to move.'
                #else:
                #    raise Exception('Invalid color to move')
                if board.is_player_to_move(request.user):
                    to_move = 'YOUR MOVE'
                else:
                    to_move = board.get_player_to_move().username + ' to move.'
                    
                if piece.piece_type == Piece.NORMAL:
                    rows[y].append('board/images/' + color + '-light.png')
                elif piece.piece_type == Piece.DEFENSE:
                    rows[y].append('board/images/' + color + '-heavy.png')
                elif piece.piece_type == Piece.OFFENSE:
                    rows[y].append('board/images/' + color + '-cavalry.png')
                else:
                    raise Exception('Invalid piece type')
                    
    return render(request, 'board/board.html', { 'rows':rows, 'to_move':to_move, 'board':board })
    