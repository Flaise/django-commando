from django.test import TestCase

from board.models import get_adjacent_positions, Piece, Color, Board

class GeneralModelTests(TestCase):

    def test_adjacency(self):
        self.assertTrue((1, 0) in get_adjacent_positions(0, 0))
        self.assertTrue((-1, 0) in get_adjacent_positions(0, 0))
        self.assertTrue((0, 1) in get_adjacent_positions(0, 0))
        self.assertTrue((0, -1) in get_adjacent_positions(0, 0))
        for x in range(-10, 11):
            for y in range(-10, 11):
                self.assertTrue((x + 1, y) in get_adjacent_positions(x, y))
                
    def test_piece_adjacency(self):
        piece = Piece()
        piece.x = 0
        piece.y = 0
        
        self.assertTrue((1, 0) in piece.get_adjacent_positions())
        self.assertTrue((-1, 0) in piece.get_adjacent_positions())
        self.assertTrue((0, 1) in piece.get_adjacent_positions())
        self.assertTrue((0, -1) in piece.get_adjacent_positions())
        
    def test_piece_is_legal_move(self):
        piece = Piece()
        piece.x = 0
        piece.y = 0
        piece.color = Color.ORANGE
        
        board = Board()
        piece.board = board
        board.color_to_move = Color.ORANGE
        
        self.assertEqual(piece.get_move_legality(1, 0), (True, None))
        self.assertEqual(piece.get_move_legality(-1, 0), (True, None))
        self.assertEqual(piece.get_move_legality(0, 1), (True, None))
        self.assertEqual(piece.get_move_legality(0, -1), (True, None))
        
    def test_piece_no_legal_moves_on_wrong_turn(self):
        piece = Piece()
        piece.x = 0
        piece.y = 0
        piece.color = Color.BLUE
        
        board = Board()
        piece.board = board
        board.color_to_move = Color.ORANGE
        
        self.assertFalse(piece.get_move_legality(1, 0)[0])
        self.assertFalse(piece.get_move_legality(-1, 0)[0])
        self.assertFalse(piece.get_move_legality(0, 1)[0])
        self.assertFalse(piece.get_move_legality(0, -1)[0])
        
    def test_piece_try_move_to(self):
        piece = Piece()
        piece.id = 1
        piece.x = 0
        piece.y = 0
        piece.piece_type = 1
        piece.color = Color.ORANGE
        
        board = Board()
        board.id = 1
        piece.board = board
        board.color_to_move = Color.ORANGE
        
        self.assertEqual(piece.try_move_to(1, 0), (True, None))
        self.assertEqual(int(piece.x), 1)
        self.assertEqual(int(piece.y), 0)
        self.assertEqual(int(board.color_to_move), Color.BLUE)
        
    def test_piece_try_move_to_offset(self):
        piece = Piece()
        piece.id = 1
        piece.x = 5
        piece.y = 5
        piece.piece_type = 1
        piece.color = Color.ORANGE
        
        board = Board()
        board.id = 1
        piece.board = board
        board.color_to_move = Color.ORANGE
        
        self.assertEqual(piece.try_move_to(6, 5), (True, None))
        self.assertEqual(int(piece.x), 6)
        self.assertEqual(int(piece.y), 5)
        self.assertEqual(int(board.color_to_move), Color.BLUE)
        
    def test_piece_try_move_to_string(self):
        piece = Piece()
        piece.id = 1
        piece.x = 0
        piece.y = 0
        piece.piece_type = 1
        piece.color = Color.ORANGE
        
        board = Board()
        board.id = 1
        piece.board = board
        board.color_to_move = Color.ORANGE
        
        self.assertEqual(piece.try_move_to('1', '0'), (True, None))
        self.assertEqual(int(piece.x), 1)
        self.assertEqual(int(piece.y), 0)
        self.assertEqual(int(board.color_to_move), Color.BLUE)