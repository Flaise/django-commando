from django.conf.urls import patterns, include, url

from board import views

urlpatterns = patterns('',
    url(r'^$', views.view_index, name='index'),
    url(r'^(?P<board_id>\d+)/$', views.view_board, name='board'),
    url(r'^(?P<board_id>\d+)/move/$', views.view_make_move, name='move'),
    url(r'^login/$', views.view_login, name='login'),
    url(r'^register/$', views.view_register, name='register'),
    url(r'^logout/$', views.view_logout, name='logout'),
    url(r'^newgame/$', views.view_newgame, name='newgame'),
    url(r'^(?P<board_id>\d+)/abandon/$', views.view_abandon, name='abandon'),
)
