# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Board'
        db.create_table(u'board_board', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('color_to_move', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'board', ['Board'])

        # Adding model 'Piece'
        db.create_table(u'board_piece', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('board', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['board.Board'])),
            ('piece_type', self.gf('django.db.models.fields.IntegerField')()),
            ('x', self.gf('django.db.models.fields.IntegerField')()),
            ('y', self.gf('django.db.models.fields.IntegerField')()),
            ('color', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'board', ['Piece'])


    def backwards(self, orm):
        # Deleting model 'Board'
        db.delete_table(u'board_board')

        # Deleting model 'Piece'
        db.delete_table(u'board_piece')


    models = {
        u'board.board': {
            'Meta': {'object_name': 'Board'},
            'color_to_move': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'board.piece': {
            'Meta': {'object_name': 'Piece'},
            'board': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['board.Board']"}),
            'color': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'piece_type': ('django.db.models.fields.IntegerField', [], {}),
            'x': ('django.db.models.fields.IntegerField', [], {}),
            'y': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['board']