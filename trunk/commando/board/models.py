from django.db import models
import datetime
from django.utils import timezone
from django.contrib.auth.models import User

class Color:
    ORANGE = 1
    BLUE = 2
    
    CHOICES = (
        (ORANGE, 'Orange'),
        (BLUE, 'Blue')
    )
    
    @staticmethod
    def get_opposite(color):
        if color == Color.ORANGE:
            return Color.BLUE
        if color == Color.BLUE:
            return Color.ORANGE
        raise Exception('Invalid color')
        
class Board(models.Model):
    #width = models.IntegerField()
    #height = models.IntegerField()
    color_to_move = models.IntegerField(choices=Color.CHOICES, default=Color.ORANGE)
    player_orange = models.ForeignKey(User, related_name='boards_orange')
    player_blue = models.ForeignKey(User, related_name='boards_blue')
    
    def __unicode__(self):
        return 'Board#' + str(self.id) + ':[' + self.player_orange.username + ' vs ' + self.player_blue.username + ']'
        
    def has_pieces_at(self, x, y):
        return len(self.get_pieces_at(x, y)) > 0
        
    def get_pieces_at(self, x, y):
        return Piece.objects.filter(board=self.pk, x=x, y=y)
        
    def get_pieces(self):
        return Piece.objects.filter(board=self.pk)
        
    def count_pieces_of_color_at(self, x, y, color):
        return len(Piece.objects.filter(board=self.pk, x=x, y=y, color=color))
        
    def is_piece_of_color_at(self, x, y, color):
        return self.count_pieces_of_color_at(x, y, color) > 0
        
    def get_piece_of_color_at(self, x, y, color):
        pieces = Piece.objects.filter(board=self.pk, x=x, y=y, color=color)
        if len(pieces) > 0:
            return pieces[0]
        return None
        
    def get_width(self):
        return 12
    def get_height(self):
        return 12
    def contains_position(self, x, y):
        return x >= 0 and y >= 0 and x < self.get_width() and y < self.get_width()
    
    def try_move_to(self, user, from_x, from_y, to_x, to_y):
        if not self.is_player_to_move(user):
            return (False, "It's not your turn to move.")
        pieces = self.get_pieces_at(from_x, from_y)
        if len(pieces) == 0:
            return (False, 'No piece selected.')
        return pieces[0].try_move_to(to_x, to_y)
        
    def is_player_to_move(self, user):
        return user == self.get_player_to_move()
        
    def get_player_to_move(self):
        if self.color_to_move == Color.ORANGE:
            return self.player_orange
        if self.color_to_move == Color.BLUE:
            return self.player_blue
        raise Exception('Invalid color to move')
        
    def populate_and_save(self):
        def make_piece_at(x, y, piece_type, color):
            piece = Piece()
            piece.x = x
            piece.y = y
            piece.piece_type = piece_type
            piece.color = color
            piece.board = self
            piece.save()
        
        self.save()
        
        make_piece_at(1, 4, Piece.NORMAL, Color.ORANGE)
        make_piece_at(2, 4, Piece.OFFENSE, Color.ORANGE)
        make_piece_at(3, 4, Piece.DEFENSE, Color.ORANGE)
        
        make_piece_at(5, 4, Piece.NORMAL, Color.ORANGE)
        make_piece_at(6, 4, Piece.NORMAL, Color.ORANGE)
        
        make_piece_at(8, 4, Piece.OFFENSE, Color.ORANGE)
        make_piece_at(9, 4, Piece.DEFENSE, Color.ORANGE)
        make_piece_at(10, 4, Piece.NORMAL, Color.ORANGE)
        
        make_piece_at(4, 2, Piece.NORMAL, Color.ORANGE)
        make_piece_at(5, 2, Piece.DEFENSE, Color.ORANGE)
        make_piece_at(6, 2, Piece.DEFENSE, Color.ORANGE)
        make_piece_at(7, 2, Piece.NORMAL, Color.ORANGE)
        
        
        make_piece_at(1, 7, Piece.NORMAL, Color.BLUE)
        make_piece_at(2, 7, Piece.DEFENSE, Color.BLUE) # heavy should be opposite cavalry
        make_piece_at(3, 7, Piece.OFFENSE, Color.BLUE)
        
        make_piece_at(5, 7, Piece.NORMAL, Color.BLUE)
        make_piece_at(6, 7, Piece.NORMAL, Color.BLUE)
        
        make_piece_at(8, 7, Piece.DEFENSE, Color.BLUE)
        make_piece_at(9, 7, Piece.OFFENSE, Color.BLUE)
        make_piece_at(10, 7, Piece.NORMAL, Color.BLUE)
        
        make_piece_at(4, 9, Piece.NORMAL, Color.BLUE)
        make_piece_at(5, 9, Piece.DEFENSE, Color.BLUE)
        make_piece_at(6, 9, Piece.DEFENSE, Color.BLUE)
        make_piece_at(7, 9, Piece.NORMAL, Color.BLUE)
        

        
adjacent_deltas = [(0, 1), (1, 0), (0, -1), (-1, 0)]
def get_adjacent_positions(x, y):
    return [(a[0] + int(x), a[1] + int(y)) for a in adjacent_deltas]

class Piece(models.Model):
    NORMAL = 1
    DEFENSE = 2
    OFFENSE = 3
    
    board = models.ForeignKey(Board)
    piece_type = models.IntegerField(choices=(
        (NORMAL, 'Light Infantry'),
        (DEFENSE, 'Heavy Infantry'),
        (OFFENSE, 'Cavalry')
    ))
    x = models.IntegerField()
    y = models.IntegerField()
    color = models.IntegerField(choices=Color.CHOICES)
    
    
    def __unicode__(self):
        #return self.get_type_name()
        return self.get_piece_type_display()
        
    #def get_type_name(self):
    #    return PieceType.get_name(self.piece_type)
        
    def count_adjacent_enemies(self):
        return sum([
            (1 if self.board.is_piece_of_color_at(a[0], a[1], Color.get_opposite(self.color)) else 0)
            for a in self.get_adjacent_positions()
        ])
    def get_adjacent_enemies(self):
        #return [self.board.get_piece_of_color_at(a[0], a[1], Color.get_opposite(self.color)) for a in self.get_adjacent_positions()]
        result = []
        for position in self.get_adjacent_positions():
            other = self.board.get_piece_of_color_at(position[0], position[1], Color.get_opposite(self.color))
            if other:
                result.append(other)
        return result
        
    def get_adjacent_positions(self):
        return get_adjacent_positions(self.x, self.y)
        
    def get_move_legality(self, dest_x, dest_y):
        dest_x = int(dest_x)
        dest_y = int(dest_y)
        
        if not self.board.contains_position(dest_x, dest_y):
            return (False, "Can't move off the board, you doofus.")
        if self.board.color_to_move != self.color:
            return (False, "It's not that player's turn to move.")
        if dest_x == self.x and dest_y == self.y:
            return (False, "Can't move in place.")
        if self.count_adjacent_enemies() > 0:
            return (False, 'That piece is engaged and cannot move because it is adjacent to the enemy.')
        if not ((dest_x, dest_y) in self.get_adjacent_positions()):
            return (False, 'Can only move to adjacent squares. (No diagonals.)')
        if self.board.has_pieces_at(dest_x, dest_y):
            return (False, "Can't move onto another piece.")
        return (True, None)
        
    def try_move_to(self, dest_x, dest_y):
        legal, fail_reason = self.get_move_legality(dest_x, dest_y)
        if not legal:
            return (legal, fail_reason)
            
        self.x = dest_x
        self.y = dest_y
        self.save()
        
        # only check adjacent enemies for possible kills because pieces don't kill themselves by walking into kill squares
        for enemy in self.get_adjacent_enemies():
            if enemy.get_current_pressure_sustained() >= enemy.get_pressure_to_kill():
                enemy.delete()
        
        if self.board.color_to_move == Color.ORANGE:
            self.board.color_to_move = Color.BLUE
        else:
            self.board.color_to_move = Color.ORANGE
        self.board.save()
        return (True, None)
        
    def get_pressure_exerted(self):
        if self.piece_type == Piece.OFFENSE:
            return 2
        else:
            return 1
            
    def get_current_pressure_sustained(self):
        result = 0
        for enemy in self.get_adjacent_enemies():
            result += enemy.get_pressure_exerted()
        return result
        
    def get_pressure_to_kill(self):
        if self.piece_type == Piece.DEFENSE:
            return 3
        else:
            return 2
        